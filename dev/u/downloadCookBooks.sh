#!/usr/bin/env bash
cd /vagrant/cookbooks
git clone https://github.com/edelight/chef-mongodb || true
git clone https://github.com/poise/python || true
git clone https://github.com/opscode-cookbooks/apt || true
git clone https://github.com/opscode-cookbooks/build-essential || true
git clone https://github.com/chef-cookbooks/yum-epel || true
git clone https://github.com/chef-cookbooks/yum || true