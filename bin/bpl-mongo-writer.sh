#!/bin/bash
BPL_BIN=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)
BPL_ROOT="$BPL_BIN"/../
BPL_LOGS=/var/log/bpl
cd "$BPL_ROOT"
nohup java -Dlogback.configurationFile="config/logback-writer.xml" -cp "config/:lib/bpl-0.1-SNAPSHOT.jar" life.bigpeka.storage.mongo.MongoWriter >$BPL_LOGS/bpl-mongo-writer.out 2>$BPL_LOGS/bpl-mongo-writer.err &