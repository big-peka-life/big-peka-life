var r = db.messages.aggregate(
    {$group:{ _id: "$channel", total: { $sum: 1}}}, 
    {$sort: {total: -1}}, 
    {$limit: 60},
    {$skip: 0});

var idlist = []
var topIdsToCounts = {}
r.forEach(function(myDoc) {
    var topId = NumberLong(myDoc._id.split("/")[1]);
    var total = myDoc.total;
    idlist.push(topId)
    topIdsToCounts[topId] = total;
});


var users = db.users.find({_id: {$in: idlist}}).toArray()
var topChannels = users.map(function(e){
    var userId = e._id;
    var userName = e.name;
    var userCounts = topIdsToCounts[userId];
    return { name: userName, counts: userCounts, id: userId};
});

function compare(a,b) {
  if (a.counts < b.counts)
    return 1;
  if (a.counts > b.counts)
    return -1;
  return 0;
}

topChannels.sort(compare);
db.topChannels.drop();
topChannels.forEach(function(e){ db.topChannels.insert(e)})

