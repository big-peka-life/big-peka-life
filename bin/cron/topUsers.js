var r = db.messages.aggregate(
    {$group:{ _id: "$from", total: { $sum: 1}}}, 
    {$sort: {total: -1}}, 
    {$limit: 63},
    {$skip: 3});

var idlist = []
var topIdsToCounts = {}
r.forEach(function(myDoc) { 
    var topId = myDoc._id.$id;
    var total = myDoc.total;
    idlist.push(topId)
    topIdsToCounts[topId] = total;
});

var users = db.users.find({_id: {$in: idlist}}).toArray()
var topUsers = users.map(function(e){
    var userId = e._id; 
    var userName = e.name;
    var userCounts = topIdsToCounts[userId];
    return { name: userName, counts: userCounts, id: userId};
});

function compare(a,b) {
  if (a.counts < b.counts)
    return 1;
  if (a.counts > b.counts)
    return -1;
  return 0;
}

topUsers.sort(compare);
db.topUsers.drop();
topUsers.forEach(function(e){ db.topUsers.insert(e)})
