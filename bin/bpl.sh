#!/bin/bash
BPL_BIN=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)
BPL_ROOT="$BPL_BIN"/../
BPL_LOGS=/var/log/bpl
cd "$BPL_ROOT"
nohup java -cp "config/:lib/bpl-0.1-SNAPSHOT.jar" life.bigpeka.BPLApplication config/bpl-config.yml >$BPL_LOGS/bpl.out 2>$BPL_LOGS/bpl.err &