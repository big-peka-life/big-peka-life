#! /bin/sh
### BEGIN INIT INFO
# Provides: bpl
# Required-Start: $remote_fs $syslog
# Required-Stop: $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: BPL
# Description: This file starts and stops BPL
#
### END INIT INFO

BPL_DIR=/usr/local/bpl/

case "$1" in
 start)
   su sck_spb -c $BPL_DIR/bin/bpl.sh
   ;;
 stop)
   su sck_spb -c jps -m | grep BPLApplication | awk '{print $1}' | xargs kill
   ;;
 restart)
   su sck_spb -c jps -m | grep BPLApplication | awk '{print $1}' | xargs kill
   su sck_spb -c $BPL_DIR/bin/bpl.sh
   ;;
 *)
   echo "Usage: bpl {start|stop|restart}" >&2
   exit 3
   ;;
esac