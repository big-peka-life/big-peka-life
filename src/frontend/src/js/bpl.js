$(document).ready(function () {
    if (location.hash.length == 0) {
        location.hash = "main";
    }
    // set up hash event listening
    handleHashChangedEvent(location.hash);
    $(window).bind('hashchange', function (e) {
        handleHashChangedEvent(location.hash);
    });
    function handleHashChangedEvent(hash) {
        console.log("handleHashChangedEvent: hash=" + hash);
        var action = routeHashUrl(hash);
    }
});

// route function decides, what action to execute and calls the function
// accepts in form of userStatistics&user=Vasya&...
function routeHashUrl(hash) {
    var hash = location.hash.replace('#', '');
    var splitHash = hash.split("/");

    var serviceName = splitHash[0];
    var serviceParams = []
    for (var i = 1; i < splitHash.length; i++) {
        serviceParams.push(splitHash[i]);
    }

    console.log("routeHashUrl: serviceName=" + serviceName);
    console.log(serviceParams);

    if (services[serviceName]) {
        console.log("routeHashUrl: executing service");
        services[serviceName](serviceName, serviceParams, hash);
    } else {
        console.error("routeHashUrl: serviceName=" + serviceName + " was not found");
    }
}
var bplApiBase = "http://bigpeka.life:8080/api/v1/";
//var bplApiBase = "http://localhost:8080/api/v1/";
var services = {
    "main": function (serviceName, params, rawHashLine) {
        switchToPage(serviceName);
        $.get(bplApiBase + "stats/general")
            .success(function (data) {
                $(".statsBlock #stats")
                    .empty()
                    .append("<li>всего сообщений - {0}</li>".format(data.totalMessages))
                    .append("<li>общающихся пользователей - {0}</li>".format(data.totalUsers))
                    .append("<li>заиндексированных стримов - {0}</li>".format(data.totalChannels));
            });
        $.get(bplApiBase + "stats/topUsers")
            .success(function (data) {
                var e = $(".statsBlock #topUsers").empty();
                data.forEach(function (de) {
                    e.append("<li><a href='/#user/{0}'>{0}</a> - {1} сообщений</li>".format(de.name, de.messageCount));
                });
            });
        $.get(bplApiBase + "stats/topChannels")
            .success(function (data) {
                var e = $(".statsBlock #topChannels").empty();
                data.forEach(function (de) {
                    e.append("<li><a href='/#channel/{0}'>{0}</a> - {1} сообщений</li>".format(de.name, de.messageCount));
                });
            });
    },
    "user": function (serviceName, params, rawHashLine) {
        if(!params[0]){
            alert("No user specified!")
            location.hash = "#main";
            switchToPage("main");
            return;
        }
        switchToPage(serviceName);
        $("#userWordCloudDiv .panel-body").empty();
        console.log("Running with rawHashLine " + rawHashLine)
        //http://bigpeka.life:8080/api/v1/userWordCounts/countsByName/LannTheStupid
        $("#userWordCloudHeading").text("Word cloud for user " + params[0] + " based on chat activity");
        var url = bplApiBase + "userWordCounts/countsByName/" + params[0];
        console.log("calling url=" + url)
        $.get(url)
            .success(function (data) {
                console.log("Got result from service")
                var words =
                    data.wordCounts
                        .map(function (o) {
                            return {text: o.id, value: o.value};
                        })
                $("#userWordCloudDiv .panel-body").empty();
                drawTheCloud(words, "#userWordCloudContainer");
            })
        $("#userWordCloudDiv .panel-body").append("<img style='margin: auto auto; position; display: block' src='/img/spiffygif_160x160.gif'>")
    },
    // copypaste
    "channel": function (serviceName, params, rawHashLine) {
        if(!params[0]){
            alert("No channel specified!")
            location.hash = "#main";
            switchToPage("main");
            return;
        }
        switchToPage(serviceName);
        $("#channelWordCloudDiv .panel-body").empty();
        $("#channelCloudHeading").text("Word cloud for channel of user " + params[0] + " based on chat activity");
        console.log("Running with rawHashLine " + rawHashLine)
        //http://bigpeka.life:8080/api/v1/userWordCounts/countsByName/LannTheStupid
        var url = bplApiBase + "channelWordCounts/" + params[0];
        console.log("calling url=" + url)
        $.get(url)
            .success(function (data) {
                console.log("Got result from service")
                var words =
                    data.wordCounts
                        .map(function (o) {
                            return {text: o.id, value: o.value};
                        })
                $("#channelWordCloudDiv .panel-body").empty();
                drawTheCloud(words, "#channelCloudContainer");
            })
        $("#channelWordCloudDiv .panel-body").append("<img style='margin: auto auto; position; display: block' src='/img/spiffygif_160x160.gif'>")
    }
}

function switchToPage(page) {
    $(".container-fluid > section").removeClass("active");
    $(".sidebar-nav a").removeClass("active");
    $("." + page + "PageReference").addClass("active");
}

function drawTheCloud(words, container) {
    var fill = d3.scale.category20();
    var wordScale = d3.scale.pow()
        .exponent(1.2)
        .domain([0, d3.max(words, function (d) {
            return d.value;
        })])
        .range([0, 15000]);
    words = words.map(function (e) {
        return {text: e.text, value: wordScale(e.value)}
    });
    var layout = d3.layout.cloud()
        .size([600, 400])
        .words(words)
        .padding(5)
        .font("Impact")
        .rotate(function () {
            return Math.round(Math.random()) * 90
        })
        .on("end", draw);
    layout.start();
    function draw(words) {
        d3.select("svg").remove();
        d3.select(container)
            .append("svg")
            .classed('wordCloud', true)
            .attr("width", layout.size()[0])
            .attr("height", layout.size()[1])
            .append("g")
            .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
            .selectAll("text")
            .data(words)
            .enter().append("text")
            .style("font-size", function (d) {
                return d.size + "px";
            })
            .style("font-family", "Impact")
            .style("fill", function (d, i) {
                return fill(i);
            })
            .attr("text-anchor", "middle")
            .attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function (d) {
                return d.text;
            });
    }
}

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}