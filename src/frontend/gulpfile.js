var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var bower = require('gulp-bower');
var gulpFilter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var minifyHTML = require('gulp-minify-html');
var minifycss = require('gulp-minify-css');
var less = require('gulp-less');
var path = require('path');

var distFolder = "../core/src/main/resources/webroot";

gulp.task('bower', function () {
    return bower()
        .pipe(gulp.dest('lib/'))
});

gulp.task('bowermin', ['bower'], function () {
    var jsFilter = gulpFilter('**/*.js');
    var cssFilter = gulpFilter('**/*.css');
    var fontFilter = gulpFilter('**/*.{ttf,woff,eof,svg,woff2}');
    return gulp.src(mainBowerFiles())
        .pipe(jsFilter)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('temp/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(distFolder))
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('temp/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(distFolder))
        .pipe(cssFilter.restore())
        .pipe(fontFilter)
        .pipe(gulp.dest(distFolder + '/fonts'));
});

/* Scripts task */
gulp.task('js', function () {
    return gulp.src(['src/js/*.js'])
        .pipe(concat('bpl.js'))
        .pipe(gulp.dest('temp/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(distFolder));
});

gulp.task('img', function () {
    return gulp.src(['src/img/*'])
        .pipe(gulp.dest(distFolder + "/img"));
});

gulp.task('html', function () {
    var opts = {
        conditionals: true,
        spare: true
    };
    return gulp.src('./src/*.html')
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest(distFolder));
});

//gulp.task('css', function() {
//    return gulp.src('./src/*.css')
//        .pipe(gulp.dest('temp/'))
//        .pipe(rename({ suffix: '.min' }))
//        .pipe(minifycss())
//        .pipe(gulp.dest(distFolder))
//});

gulp.task('less', function () {
    return gulp.src('src/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('temp/'))
        .pipe(concat('bpl.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(distFolder))
});

/* Reload task */
gulp.task('bs-reload', function () {
    browserSync.reload();
});

/* Prepare Browser-sync for localhost */
gulp.task('browser-sync', function () {
    browserSync.init({
        server: distFolder,
        port: 1488
    });
});

gulp.task('build', ['js', 'html', 'less', 'img', 'bowermin']);


/* Watch src files, building them with separate tasks. require browser-sync and bowermin */
gulp.task('default', ['browser-sync', 'build'], function () {
    /* Watch app.js file, run the scripts task on change. */
    gulp.watch(['src/js/*.js'], ['js', 'bs-reload'])
    gulp.watch(['src/*.html'], ['html', 'bs-reload'])
    gulp.watch(['src/less/**/*.less'], ['less', 'bs-reload'])
    gulp.watch(['bower.json'], ['bowermin', 'bs-reload'])
});