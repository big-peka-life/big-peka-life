package life.bigpeka;

import com.pholser.junit.quickcheck.generator.*;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;
import java.lang.reflect.AnnotatedType;

public class AfroGenerator extends Generator<AfroAmerican> {
  public AfroGenerator(final Class<AfroAmerican> type) {
    super(type);
  }

  public AfroGenerator() {
    super(AfroAmerican.class);
  }

  @Override
  public AfroAmerican generate(final SourceOfRandomness sourceOfRandomness,
      final GenerationStatus generationStatus) {
    return new AfroAmerican(sourceOfRandomness.nextInt(12, 21));
  }

  @Override
  public void configure(final AnnotatedType annotatedType) {
    super.configure(annotatedType);
  }
}
