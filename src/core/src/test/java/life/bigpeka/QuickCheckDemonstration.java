package life.bigpeka;

import com.pholser.junit.quickcheck.*;
import com.pholser.junit.quickcheck.generator.*;
import org.junit.contrib.theories.*;
import org.junit.runner.RunWith;
import java.io.Serializable;
import java.util.Map;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assume.*;

@RunWith(Theories.class)
public class QuickCheckDemonstration {

  @Theory
  public void doTheStuff(@ForAll String foo, @ForAll int bar) {
    System.out.println(foo);
    System.out.println(bar);
  }

  @Theory
  public void doTheStuff2(@ForAll Foo foo) {
    System.out.println(foo.foo(1488));
  }

  @Theory
  public void doTheStuff3(@ForAll Map<Integer, String> foo) {
    System.out.println(foo.size());
  }

  @Theory
  public void doTheStuff4(@ForAll(sampleSize = 23) Pussy pussy) {
    System.out.println(pussy);
  }

  @Theory public void doTheStuff5(@ForAll @From(AfroGenerator.class) AfroAmerican pussy) {
    System.out.println(pussy);
  }

  @Theory
  public void doTheStuff6(@ForAll @InRange(min = "14", max = "88") int dickSize) {
    assumeThat(dickSize, greaterThan(14));
    assumeThat(dickSize, lessThan(88));
    System.out.println(dickSize);
  }
}

@FunctionalInterface
interface Foo {
  public String foo(int i);
}

enum Pussy {
  SMALL,
  BIG,
  GIANT
}

class AfroAmerican implements Serializable{
  public int dickLength;

  public AfroAmerican(final int dickLength) {
    this.dickLength = dickLength;
  }

  public ThreeProjections getThreeProjections() {
    return new ThreeProjections();
  }

  @Override
  public String toString() {
    return "AfroAmerican{" +
        "dickLength=" + dickLength +
        '}';
  }
}

class ThreeProjections{

}