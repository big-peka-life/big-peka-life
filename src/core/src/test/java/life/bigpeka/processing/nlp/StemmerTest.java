package life.bigpeka.processing.nlp;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import static org.assertj.core.api.Assertions.assertThat;

public class StemmerTest {
  @Test
  public void shouldStem() {
    final ArrayList<String> input = Lists.newArrayList("стол", "стола", "столу", "столешница");
    final ArrayList<String> stem = Stemmer.stem(input);
    assertThat(stem).isEqualTo(Lists.newArrayList("стол", "стол", "стол", "столешниц"));
  }
  @Test
  public void shouldYoStem() {
    final ArrayList<String> input = Lists.newArrayList("паёк", "ёлка", "ёжик");
    final ArrayList<String> stem = Stemmer.stem(input);
    assertThat(stem).isEqualTo(input);
  }
  @Test
  public void shouldPluralStem() {
    final ArrayList<String> input = Lists.newArrayList("машина", "кровати", "деревья");
    final ArrayList<String> stem = Stemmer.stem(input);
    assertThat(stem).isEqualTo(Lists.newArrayList("машин", "кроват", "дерев"));
  }
  @Test
  public void shouldLastsymbolStem() {
    final ArrayList<String> input = Lists.newArrayList("мед", "моль", "киль");
    final ArrayList<String> stem = Stemmer.stem(input);
    assertThat(stem).isEqualTo(Lists.newArrayList("мед", "мол", "кил"));
  }
  @Test
  public void shouldEnglishStem() {
    final ArrayList<String> input = Lists.newArrayList("peka", "streaming", "flowers");
    final ArrayList<String> stem = Stemmer.stem(input);
    assertThat(stem).isEqualTo(Lists.newArrayList("peka", "streaming", "flowers"));
  }
  @Test
  public void shouldPassAuthorsTestStem() {
    final ArrayList<String> input = Lists.newArrayList("августа", "захочется", "звезды", "звуки", "сконфузившаяся",
            "плющом", "поганая", "погорел", "просьба", "ящик");
    final ArrayList<String> stem = Stemmer.stem(input);
    assertThat(stem).isEqualTo(Lists.newArrayList("август", "захочет", "звезд", "звук",
        "сконфуз", "плющ", "поган", "погорел", "просьб", "ящик"));
  }
}

