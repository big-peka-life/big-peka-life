package life.bigpeka.processing.nlp;

import com.google.common.collect.Lists;
import org.junit.Test;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class StopWordsCleanerTest {
  @Test
  public void shouldCleanInPlaceStopWords() throws Exception {
    final ArrayList<String> srcWords = Lists.newArrayList(
        "слава", "украине", "героям", "путин", "была", "даром", "лишь");
    StopWordsCleaner.cleanListInplace(srcWords);
    final ArrayList<String> target = Lists.newArrayList(
        "слава", "украине", "героям", "путин");
    assertThat(target).isEqualTo(srcWords);
  }

  @Test
  public void shouldCleanStopWords() throws Exception {
    ArrayList<String> srcString = Lists.newArrayList(
            "слава", "украине", "героям", "путин", "была", "даром", "лишь",
            "кое", "как", "кой", "куда", "даваи", "ка", "как", "либо", "как", "нибудь", "за", "то");
    ArrayList<String> cleaned = StopWordsCleaner.clean(srcString);
    assertThat(cleaned).isEqualTo(Lists.newArrayList(
            "слава", "украине", "героям", "путин"));
  }
}