package life.bigpeka.processing.nlp;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PreProcessorTest {
  @Test
  public void shouldPreProcessUpperCasesAndExtraWhitespaces() throws Exception {
      final String srcString = "Aa   aaAAa A a A Ёё Йй";
      final List<String> resultingString = PreProcessor.preProcess(srcString);
      assertThat(resultingString)
          .isEqualTo(Lists.newArrayList("aa", "aaaaa", "a", "a", "a", "ее", "ии"));
  }

  @Test
  public void shouldRemoveBlockedCharacters() throws Exception {
    final String srcString = "Aa ( ͡° ͜ʖ ͡°) ¯\\_(ツ)_/¯  !!!  aaAAa A a A , . [";
    final List<String> resultingString = PreProcessor.preProcess(srcString);
    assertThat(resultingString)
        .isEqualTo(Lists.newArrayList("aa", "aaaaa", "a", "a", "a"));
  }

}