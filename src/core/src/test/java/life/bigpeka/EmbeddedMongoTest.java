package life.bigpeka;

import com.google.common.collect.*;
import com.mongodb.*;
import de.flapdoodle.embed.mongo.*;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.distribution.Version;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.mongodb.morphia.*;
import org.mongodb.morphia.annotations.*;
import org.mongodb.morphia.query.Query;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.*;
import static org.assertj.core.api.Assertions.assertThat;

public class EmbeddedMongoTest {

  @Test
  public void shouldBeFunctional()
  throws UnknownHostException {
    int port = 12345;
    MongodConfig mongodConfig = new MongodConfig(Version.Main.V2_3, port, false);

    MongodStarter runtime = MongodStarter.getDefaultInstance();

    MongodExecutable mongodExecutable = null;
    try {
      mongodExecutable = runtime.prepare(mongodConfig);
      MongodProcess mongod = mongodExecutable.start();

      final Morphia morphia = new Morphia().map(MongoPersistedEntity.class);
      final Datastore datastore = morphia.createDatastore(
          new MongoClient("localhost", port),
          "test_db");
      datastore.ensureIndexes();
      final List<Integer> ints = Lists.newArrayList(1, 2, 3, 4, 5);
      datastore.save(new MongoPersistedEntity(ObjectId.get(), "test", ints));

      final Query<MongoPersistedEntity> q = datastore.createQuery(MongoPersistedEntity.class);
      final List<MongoPersistedEntity> found =
          q.field("name").equal("test")
              .asList();

      System.out.println("/////////////////////" + found);
      assertThat(found.get(0).shit).isEqualTo(Lists.newArrayList(1, 2, 3, 4, 5));
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (mongodExecutable != null)
        mongodExecutable.stop();
    }
  }
}

@Entity("employees")
class MongoPersistedEntity {
  @Id
  public ObjectId id;

  public String name;

  public List<Integer> shit;

  public MongoPersistedEntity(final ObjectId id, final String name, final List<Integer> shit) {
    this.id = id;
    this.name = name;
    this.shit = shit;
  }

  public MongoPersistedEntity() { }

  @Override
  public String toString() {
    return "MongoPersistedEntity{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", shit=" + shit +
        '}';
  }
}