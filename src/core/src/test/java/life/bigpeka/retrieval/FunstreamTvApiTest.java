package life.bigpeka.retrieval;

import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class FunstreamTvApiTest {

  @Test
  public void shouldRetrieveListOfStreams() {
    final FunstreamTvApi funstreamTvApi = FunstreamTvApi.factories.create();
    final Streams streams = funstreamTvApi
        .getStreams(new StreamsRequest("top", "stream", "all"));
    assertThat(streams.getStreams().size()).isGreaterThan(14);
  }

  @Test
  public void shouldRetrieveExistingStreamInfo() {
    final FunstreamTvApi funstreamTvApi = FunstreamTvApi.factories.create();
    final Streams streams = funstreamTvApi
        .getStreams(new StreamsRequest("top", "stream", "all"));
    final Stream stream = streams.getStreams().get(0);
    final Stream stream1 = funstreamTvApi.getStream(new StreamRequest(stream.getId()));
    assertThat(stream1.getId()).isEqualTo(stream.getId());
    assertThat(stream1.getStreamer()).isEqualTo(stream.getStreamer());
    System.out.println(stream1);
  }
}