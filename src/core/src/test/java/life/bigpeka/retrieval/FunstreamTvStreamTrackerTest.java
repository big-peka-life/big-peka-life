package life.bigpeka.retrieval;

import java.util.concurrent.*;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class FunstreamTvStreamTrackerTest {
  @Test
  public void should()
  throws ExecutionException, InterruptedException {
    final CompletableFuture<Stream> awaitedStreamName = new CompletableFuture<Stream>();
    new FunstreamTvStreamTracker()
        .onStreamOnline(s -> awaitedStreamName.complete(s))
        .start(1488L, TimeUnit.MILLISECONDS);
    final String streamName = awaitedStreamName.get().getName();
    assertThat(streamName).isNotNull();
    assertThat(streamName.length()).isGreaterThan(1);
  }
}