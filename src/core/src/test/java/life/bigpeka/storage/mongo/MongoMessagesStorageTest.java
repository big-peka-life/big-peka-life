//package life.bigpeka.storage.mongo;
//
//import com.google.common.collect.Lists;
//import com.mongodb.MongoClient;
//import de.flapdoodle.embed.mongo.*;
//import de.flapdoodle.embed.mongo.config.MongodConfig;
//import de.flapdoodle.embed.mongo.distribution.Version;
//import life.bigpeka.*;
//import life.bigpeka.storage.csv.CSVReader;
//import org.junit.*;
//import java.io.IOException;
//import java.util.List;
//import static org.assertj.core.api.Assertions.*;
//
//public class MongoMessagesStorageTest {
//
//  MongodProcess mongod;
//
//  MongoClient mongoClient;
//
//  @Before
//  public void startMongo() {
//    int port = 12345;
//    MongodConfig mongodConfig = new MongodConfig(Version.Main.V2_3, port, false);
//    MongodStarter runtime = MongodStarter.getDefaultInstance();
//    MongodExecutable mongodExecutable = runtime.prepare(mongodConfig);
//    try {
//      mongodExecutable = runtime.prepare(mongodConfig);
//      mongod = mongodExecutable.start();
//      mongoClient = new MongoClient("localhost", port);
//    } catch (IOException e) {
//      throw new IllegalStateException(e);
//    }
//  }
//
//  @After
//  public void stopMongo() {
//    mongod.stop();
//  }
//
//  @Test
//  public void shouldMapAndWriteReadTestMessages() {
//    final MongoMessagesStorage mongoMessagesStorage = new MongoMessagesStorage(mongoClient);
//    mongoMessagesStorage.open();
//    final List<Message> messages = new CSVReader().readFile(SampleMessages.getAsFile());
//    messages.forEach(mongoMessagesStorage::addMessage);
//    final long count = mongoClient.getDatabase("messages_db").getCollection("messages").count();
//    assertThat(count).isEqualTo(messages.size());
//    mongoMessagesStorage.close();
//  }
//}
