package life.bigpeka;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.*;

@RunWith(Parameterized.class)
public class JunitParameterizedDemonstration {

  private final int i;

  private final int j;

  public JunitParameterizedDemonstration(int i, int j) {
    this.i = i;
    this.j = j;
  }

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {0, 0}, {1, 1}, {2, 1}, {3, 2}, {4, 3}, {5, 5}, {6, 8}
    });
  }

  @Test
  public void test() {
    System.out.println(i + " " + j);
  }

}
