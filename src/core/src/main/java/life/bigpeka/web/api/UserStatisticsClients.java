package life.bigpeka.web.api;

import feign.*;
import feign.jackson.*;
import feign.jaxrs.JAXRSContract;
import feign.slf4j.Slf4jLogger;

public class UserStatisticsClients {
  public static UserStatisticsApi bindTo(String host, int port) {
    return Feign.builder()
        .contract(new JAXRSContract())
        .decoder(new JacksonDecoder())
        .encoder(new JacksonEncoder())
        .logger(new Slf4jLogger())
        .logLevel(Logger.Level.BASIC)
        .target(UserStatisticsApi.class, "http://" + host + ":" + port);
  }

  public static UserStatisticsApi bindToDefault() {
    return bindTo("localhost", 8080);
  }
}
