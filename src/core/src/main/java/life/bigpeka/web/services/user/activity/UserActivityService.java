package life.bigpeka.web.services.user.activity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.slf4j.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/v1/userActivity/")
public class UserActivityService {
  private ArrayList<String> topUsers;
  private ArrayList<Integer> postTopUsers;
  private ArrayList<String> listOfTime;
  private ArrayList<Integer> listOfFrequency;
  private String resultTop10;
  private String resultTimeActivityUserName;

  @Inject
  public UserActivityService(UserActivityDataProvider userActivityDataProvider) {
    topUsers = userActivityDataProvider.getTopUsers();
    postTopUsers = userActivityDataProvider.getPostTopUsers();
    listOfTime = userActivityDataProvider.getListOfTime();
    listOfFrequency = userActivityDataProvider.getListOfFrequency();
  }

  final static Logger logger = LoggerFactory.getLogger(UserActivity.class);
  private ObjectMapper mapper = new ObjectMapper();

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/top10")
  public UserActivity topUserActivity() {
    logger.info("Processing timeActivityTop10 request for top10");
    UserActivity userActivity = new UserActivity(topUsers, postTopUsers);
    try {
      resultTop10 = mapper.writeValueAsString(userActivity);
    } catch (JsonProcessingException e) {
      logger.error("Can't write value in top10", e);
    }
    return userActivity;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/timeActivity/{userName}")
  public TimeActivity timeActivity(@PathParam("userName") String userName) {
    logger.info("Processing timeActivityUserName request for {}", userName);
    TimeActivity timeActivity = new TimeActivity(userName, listOfTime, listOfFrequency);
    try {
      resultTimeActivityUserName = mapper.writeValueAsString(timeActivity);
    } catch (JsonProcessingException e) {
      logger.error("Can't write value in timeActivity/{userName}", e);
    }
    return timeActivity;
  }
}
