package life.bigpeka.web.api;

import life.bigpeka.web.services.user.statistics.UserStatistics;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("api/v1/userStatistics")
public interface UserStatisticsApi {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("byName/{userName}")
  public UserStatistics getForUserId(@PathParam("userName") String userName);
}
