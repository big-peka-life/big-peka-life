package life.bigpeka.web.services.user.activity;

import com.google.common.collect.Lists;
import java.util.ArrayList;

public class UserActivityDataProvider {

  private ArrayList<String> topUsers = Lists.newArrayList("Vasya", "Lil", "Ibah", "Leon", "Nagib");
  private ArrayList<Integer> postTopUsers = Lists.newArrayList(1500, 1300, 1000, 974, 647);
  private ArrayList<String> listOfTime = Lists.newArrayList("1434608971", "1434608986",
      "1434609214");
  private ArrayList<Integer> listOfFrequency = Lists.newArrayList(3,6,9);

  public ArrayList<String> getTopUsers() {
    return topUsers;
  }

  public ArrayList<Integer> getPostTopUsers() {
    return postTopUsers;
  }

  public ArrayList<String> getListOfTime() {
    return listOfTime;
  }

  public ArrayList<Integer> getListOfFrequency() {
    return listOfFrequency;
  }

  public void setTopUsers(final ArrayList<String> topUsers) {
    this.topUsers = topUsers;
  }

  public void setPostTopUsers(final ArrayList<Integer> postTopUsers) {
    this.postTopUsers = postTopUsers;
  }

  public void setListOfTime(final ArrayList<String> listOfTime) {
    this.listOfTime = listOfTime;
  }

  public void setListOfFrequency(final ArrayList<Integer> listOfFrequency) {
    this.listOfFrequency = listOfFrequency;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final UserActivityDataProvider that = (UserActivityDataProvider) o;

    if (listOfFrequency != null ? !listOfFrequency.equals(that.listOfFrequency) :
        that.listOfFrequency != null) { return false; }
    if (listOfTime != null ? !listOfTime.equals(that.listOfTime) : that.listOfTime != null) {
      return false;
    }
    if (postTopUsers != null ? !postTopUsers.equals(that.postTopUsers) :
        that.postTopUsers != null) {
      return false;
    }
    if (topUsers != null ? !topUsers.equals(that.topUsers) : that.topUsers != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = topUsers != null ? topUsers.hashCode() : 0;
    result = 31 * result + (postTopUsers != null ? postTopUsers.hashCode() : 0);
    result = 31 * result + (listOfTime != null ? listOfTime.hashCode() : 0);
    result = 31 * result + (listOfFrequency != null ? listOfFrequency.hashCode() : 0);
    return result;
  }
}
