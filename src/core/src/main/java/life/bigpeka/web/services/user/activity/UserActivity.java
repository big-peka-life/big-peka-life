package life.bigpeka.web.services.user.activity;

import java.util.ArrayList;

public class UserActivity {

  private ArrayList<String> topUsers;
  private ArrayList<Integer> postTopUsers;

  public UserActivity(final ArrayList<String> topUsers, final ArrayList<Integer> postTopUsers) {
    this.topUsers = topUsers;
    this.postTopUsers = postTopUsers;
  }

  public ArrayList<String> getTopUsers() {
    return topUsers;
  }

  public ArrayList<Integer> getPostTopUsers() {
    return postTopUsers;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final UserActivity that = (UserActivity) o;

    if (postTopUsers != null ? !postTopUsers.equals(that.postTopUsers) :
        that.postTopUsers != null) {
      return false;
    }
    if (topUsers != null ? !topUsers.equals(that.topUsers) : that.topUsers != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = topUsers != null ? topUsers.hashCode() : 0;
    result = 31 * result + (postTopUsers != null ? postTopUsers.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UserActivity{" +
        "topUsers=" + topUsers +
        ", postTopUsers=" + postTopUsers +
        '}';
  }
}
