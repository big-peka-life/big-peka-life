package life.bigpeka.web.services.user.activity;

import java.util.ArrayList;

public class TimeActivity {
  private String userName;
  private ArrayList<String> listOfTime;
  private ArrayList<Integer> listOfFrequency;

  public TimeActivity(final String userName, final ArrayList<String> listOfTime, final
  ArrayList<Integer>
      listOfFrequency) {
    this.userName = userName;
    this.listOfTime = listOfTime;
    this.listOfFrequency = listOfFrequency;
  }

  public ArrayList<String> getListOfTime() {
    return listOfTime;
  }

  public ArrayList<Integer> getListOfFrequency() {
    return listOfFrequency;
  }

  public String getUserName() {
    return userName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final TimeActivity that = (TimeActivity) o;

    if (listOfFrequency != null ? !listOfFrequency.equals(that.listOfFrequency) :
        that.listOfFrequency != null) { return false; }
    if (listOfTime != null ? !listOfTime.equals(that.listOfTime) : that.listOfTime != null) {
      return false;
    }
    if (userName != null ? !userName.equals(that.userName) : that.userName != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = userName != null ? userName.hashCode() : 0;
    result = 31 * result + (listOfTime != null ? listOfTime.hashCode() : 0);
    result = 31 * result + (listOfFrequency != null ? listOfFrequency.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "TimeActivity{" +
        "userName='" + userName + '\'' +
        ", listOfTime=" + listOfTime +
        ", listOfFrequency=" + listOfFrequency +
        '}';
  }
}
