package life.bigpeka.web.services.wordcounts;

enum ResponseType {
  DATA_RETRIEVED,
  NO_DATA
}
