package life.bigpeka.web.services.user.statistics;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/v1/userStatistics")
public class UserStatisticsService {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/byName/{userName}")
  public UserStatistics getForUserId(@PathParam("userName") String userName) {
    return new UserStatistics(userName, 1487, 5000);
  }
}
