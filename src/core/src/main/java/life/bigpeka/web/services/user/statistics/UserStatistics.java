package life.bigpeka.web.services.user.statistics;

public class UserStatistics {
  private String name;

  private Integer pekaCount;

  private Integer allPeks;

  public UserStatistics(final String name, final Integer pekaCount, final Integer allPeks) {
    this.name = name;
    this.pekaCount = pekaCount;
    this.allPeks = allPeks;
  }

  public UserStatistics() {
  }

  public String getName() {
    return name;
  }

  public Integer getPekaCount() {
    return pekaCount;
  }

  public Integer getAllPeks() {
    return allPeks;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final UserStatistics that = (UserStatistics) o;

    if (allPeks != null ? !allPeks.equals(that.allPeks) : that.allPeks != null) { return false; }
    if (name != null ? !name.equals(that.name) : that.name != null) { return false; }
    if (pekaCount != null ? !pekaCount.equals(that.pekaCount) : that.pekaCount != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (pekaCount != null ? pekaCount.hashCode() : 0);
    result = 31 * result + (allPeks != null ? allPeks.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UserStatistics{" +
        "name='" + name + '\'' +
        ", pekaCount=" + pekaCount +
        ", allPeks=" + allPeks +
        '}';
  }
}
