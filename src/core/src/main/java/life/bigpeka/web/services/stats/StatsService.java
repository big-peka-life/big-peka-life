package life.bigpeka.web.services.stats;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.*;
import life.bigpeka.*;
import org.mongodb.morphia.Datastore;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.stream.Collectors;

@Path("/v1/stats/")
public class StatsService {

  @Inject
  Datastore dataStore;

  @GET
  @Path("/general")
  @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
  public GeneralStats generalStats() {
    return new GeneralStats(
        dataStore.getCount(Message.class),
        dataStore.getCount(User.class),
        dataStore.getCount(Stream.class));
  }

  @GET
  @Path("/topUsers")
  @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
  public List<TopEntry> topUsers() {
    return topEntries("topUsers");

  }

  @GET
  @Path("/topChannels")
  @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
  public List<TopEntry> topChannels() {
    return topEntries("topChannels");
  }


  List<TopEntry> topEntries(String collectionName){
    final DBCollection topUsers = dataStore.getDB().getCollectionFromString(collectionName);
    final List<DBObject> dbObjects = topUsers.find().toArray();
    return dbObjects.stream().map(dbObject -> {
      final Long id = (Long)dbObject.get("id");
      final String name = (String) dbObject.get("name");
      final Integer counts = Double.valueOf((Double) dbObject.get("counts")).intValue();
      return new TopEntry(id, name, counts);
    }).collect(Collectors.toCollection(ArrayList::new));
  }
}


class GeneralStats{
  public long totalMessages;
  public long totalUsers;
  public long totalChannels;

  public GeneralStats(final long totalMessages, final long totalUsers, final long totalChannels) {
    this.totalMessages = totalMessages;
    this.totalUsers = totalUsers;
    this.totalChannels = totalChannels;
  }
}

class TopEntry {
  public Long id;
  public String name;
  public Integer messageCount;
  public TopEntry(final Long id, final String name, final Integer messageCount) {
    this.id = id;
    this.name = name;
    this.messageCount = messageCount;
  }
}