package life.bigpeka.web.services.wordcounts;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.*;
import life.bigpeka.*;
import org.mongodb.morphia.*;
import org.slf4j.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.*;
import java.util.stream.Collectors;

@Path("/v1/channelWordCounts")
public class ChannelWordCountsService {
  private static Logger LOGGER = LoggerFactory.getLogger(UserWordCountsService.class);

  @Inject
  Datastore dataStore;

  @GET
  @Path("/{streamerName}")
  @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
  public Response byStreamerName(
      @PathParam("streamerName") String streamerName) {
    LOGGER.info("Processing byStreamerName request for {}", streamerName);
    ArrayList<WordCount> wordCounts;
    ResponseType responseType;
    try {
      wordCounts = topWordsForStreamer(streamerName);
      if (wordCounts.size() > 0) {
        responseType = ResponseType.DATA_RETRIEVED;
      } else {
        responseType = ResponseType.NO_DATA;
      }
      return Response.ok(new WordCountsResponse(responseType, wordCounts)).build();
    } catch (Exception anyException) {
      wordCounts = Lists.newArrayList();
      responseType = ResponseType.NO_DATA;
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
          .entity(new WordCountsResponse(responseType, wordCounts))
          .build();
    }
  }

  private ArrayList<WordCount> topWordsForStreamer(String name) {
    final User user = dataStore.find(User.class)
        .field("name").equal(name).get();
    final List<Stream> stream = dataStore.find(Stream.class)
        .field("streamer").equal(new DBRef("users", user.getId())).asList();
    final Stream mainStream = stream.get(0);
    if (mainStream != null) {
      // filter for channel for this user assuming it exists (mainStream)
      final DBObject query = BasicDBObjectBuilder
          .start().add("channel", "stream/" + user.getId()).get();
      final MapReduceCommand countsForChannel =
          new MapReduceCommand(dataStore.getCollection(Message.class),
              "function(){ this.stems && this.stems.forEach(function(i){ emit(i, 1) }); }",
              "function(word,one){ return Array.sum(one); };",
              null,
              MapReduceCommand.OutputType.INLINE,
              query);

      final MapreduceResults<WordCount> mrResults = dataStore.mapReduce(MapreduceType.INLINE,
          dataStore.createQuery(Message.class)
              .field("channel").equal("stream/" + user.getId()),
          WordCount.class,
          countsForChannel);
      return Lists.newArrayList(mrResults.iterator())
          .stream().sorted((o1, o2) -> o2.value - o1.value)
          .limit(15)
          .collect(Collectors.toCollection(ArrayList::new));
    } else {
      return Lists.newArrayList();
    }
  }
}
