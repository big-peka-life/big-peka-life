package life.bigpeka.web.services.wordcounts;

import java.util.List;

class WordCountsResponse {
  public ResponseType responseType;

  public List<WordCount> wordCounts;

  public WordCountsResponse(final ResponseType responseType,
      final List<WordCount> wordCounts) {
    this.responseType = responseType;
    this.wordCounts = wordCounts;
  }
}
