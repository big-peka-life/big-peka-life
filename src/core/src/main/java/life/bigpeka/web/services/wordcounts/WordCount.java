package life.bigpeka.web.services.wordcounts;

import org.mongodb.morphia.annotations.Id;

class WordCount {
  @Id
  public String id;

  public int value;
}
