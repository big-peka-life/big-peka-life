package life.bigpeka.web.services.wordcounts;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.*;
import life.bigpeka.*;
import org.mongodb.morphia.*;
import org.slf4j.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Path("/v1/userWordCounts")
public class UserWordCountsService {
  private static Logger LOGGER = LoggerFactory.getLogger(UserWordCountsService.class);

  @Inject
  Datastore dataStore;

  @GET
  @Path("/countsByName/{userName}")
  @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
  public Response countsByName(@PathParam("userName") String userName) {
    LOGGER.info("Processing countsByName request for {}", userName);
    ArrayList<WordCount> wordCounts;
    ResponseType responseType;
    try {
      wordCounts = topWordsFor(userName);
      if (wordCounts.size() > 0) {
        responseType = ResponseType.DATA_RETRIEVED;
      } else {
        responseType = ResponseType.NO_DATA;
      }
      return Response.ok(new WordCountsResponse(responseType, wordCounts)).build();
    } catch (Exception anyException) {
      wordCounts = Lists.newArrayList();
      responseType = ResponseType.NO_DATA;
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
          .entity(new WordCountsResponse(responseType, wordCounts))
          .build();
    }
  }

  @GET
  @Path("/cloudByName/{userName}")
  @Produces("image/png")
  public Response cloudByName(@PathParam("userName") String userName) {
    LOGGER.info("Processing countsByName request for {}", userName);
    return Response.seeOther(URI.create("/#userWordCounts/cloudByName/" + userName)).build();
  }

  private ArrayList<WordCount> topWordsFor(String name) {
    final User user = dataStore.find(User.class)
        .field("name").equal(name).get();
    if (user != null) {
      final DBObject query = BasicDBObjectBuilder
          .start().add("from", new DBRef("users", user.getId())).get();
      final MapReduceCommand mrt77 = new MapReduceCommand(dataStore.getCollection(Message.class),
          "function(){ this.stems && this.stems.forEach(function(i){ emit(i, 1) }); }",
          "function(word,one){ return Array.sum(one); };",
          null,
          MapReduceCommand.OutputType.INLINE,
          query);
      //    final MapReduceOutput mapReduceOutput = messageCollection.mapReduce(mrt77);
      //    System.out.println(mapReduceOutput);

      final MapreduceResults<WordCount> mrResults = dataStore.mapReduce(MapreduceType.INLINE,
          dataStore.createQuery(Message.class)
              .field("from").equal(new DBRef("users", user.getId())),
          WordCount.class,
          mrt77);
      return Lists.newArrayList(mrResults.iterator())
          .stream().sorted((o1, o2) -> o2.value - o1.value)
          .limit(15)
          .collect(Collectors.toCollection(ArrayList::new));
    } else {
      return Lists.newArrayList();
    }
  }
}


