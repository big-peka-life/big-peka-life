package life.bigpeka;

import com.google.inject.*;
import com.mongodb.MongoClient;
import org.mongodb.morphia.*;
import org.mongodb.morphia.logging.MorphiaLoggerFactory;
import org.mongodb.morphia.logging.slf4j.SLF4JLoggerImplFactory;

public class BPLModule extends AbstractModule {
  @Override
  protected void configure() {
  }

  @Provides
  @Singleton
  Datastore datastore() {
    MorphiaLoggerFactory.registerLogger(SLF4JLoggerImplFactory.class);
    final MongoClient mongoClient = new MongoClient("nicepussy.ru");
    final Morphia morphia = new Morphia().map(Message.class, User.class, Stream.class);
    Datastore datastore = morphia.createDatastore(mongoClient, "messages_db");
    datastore.ensureIndexes();
    return datastore;
  }
}
