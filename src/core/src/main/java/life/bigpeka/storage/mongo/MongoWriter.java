package life.bigpeka.storage.mongo;

import com.mongodb.MongoClient;
import life.bigpeka.Message;
import life.bigpeka.retrieval.*;
import org.slf4j.*;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class MongoWriter {
  private final static Logger LOGGER = LoggerFactory.getLogger(MongoWriter.class);

  public static void main(String[] args)
  throws IOException {
    final MongoMessagesStorage mongoMessagesStorage =
        new MongoMessagesStorage(new MongoClient("nicepussy.ru"));
    mongoMessagesStorage.open();

    FunstreamTvClient client = new FunstreamTvClient(new FunstreamTvEventListener() {
      @Override
      public void onConnect() {

      }

      @Override
      public void onDisconnect() {

      }

      @Override
      public void onMessage(final Message message) {
        mongoMessagesStorage.addMessage(message);
      }

      @Override
      public void onJoin(final String channel) {

      }
    }, Collections.singletonList("main"));

    client.connect();

    new FunstreamTvStreamTracker()
        .onStreamOnline(s -> {
          client.join(s);
          LOGGER.info("Joined stream " + s);
        })
        .onStreamOffline(s -> {
          client.leave(s);
          LOGGER.info("Left stream " + s);
        }).start(10, TimeUnit.SECONDS);
  }
}
