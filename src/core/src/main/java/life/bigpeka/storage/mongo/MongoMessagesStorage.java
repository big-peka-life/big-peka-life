package life.bigpeka.storage.mongo;

import com.mongodb.MongoClient;
import life.bigpeka.*;
import life.bigpeka.storage.MessagesStorage;
import org.mongodb.morphia.*;

public class MongoMessagesStorage implements MessagesStorage {

  private final MongoClient client;

  private Datastore datastore;

  public MongoMessagesStorage(MongoClient client) {
    this.client = client;
    final Morphia morphia = new Morphia().map(Message.class, User.class, Stream.class);
    datastore = morphia.createDatastore(client, "messages_db");
    datastore.ensureIndexes();
  }

  @Override
  public void open() {
  }

  @Override
  public void addMessage(final Message message) {
    datastore.save(message);
    datastore.save(message.getFrom());
    datastore.save(message.getStream());
    if (message.getStream().getStreamer() != null) {
      datastore.save(message.getStream().getStreamer());
    }
    if (message.getTo() != null) {
      datastore.save(message.getTo());
    }
  }

  @Override
  public void close() {
    client.close();
  }
}
