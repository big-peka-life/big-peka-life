package life.bigpeka.storage;

import life.bigpeka.Message;

@FunctionalInterface
public interface MessagesStorage {
  default void open(){ }

  void addMessage(Message message);

  default void close(){}
}
