package life.bigpeka.retrieval;

import life.bigpeka.Message;

@FunctionalInterface
public interface FunstreamTvEventListener {
  default void onConnect() { }

  default void onDisconnect() { }

  void onMessage(Message message);

  default void onHistoryMessage(Message message) {
    onMessage(message);
  }

  default void onJoin(String channel) { }
}