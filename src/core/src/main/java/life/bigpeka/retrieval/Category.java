package life.bigpeka.retrieval;

public class Category {
  /* not complete implementation, example of full object for response
    {
      contentAmount: 165
      id: 1
      name: ""
      slug: "top"
      subCategories: [{id: 3, slug: "letsplay", name: "Игры", contentAmount: 104, weight: 7870},…]
      weight: 9837
    }

    for request only `slug` is needed
   */

  private final String slug;

  public Category(String slug) {
    this.slug = slug;
  }

  public String getSlug() {
    return slug;
  }

  @Override
  public String toString() {
    return "Category{" +
        "slug='" + slug + '\'' +
        '}';
  }
}
