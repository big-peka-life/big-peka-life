package life.bigpeka.retrieval;

import com.github.nkzawa.socketio.client.*;
import com.google.common.collect.Maps;
import life.bigpeka.*;
import life.bigpeka.processing.nlp.NLPProcessor;
import org.json.*;
import org.slf4j.*;
import javax.validation.constraints.NotNull;
import java.net.URISyntaxException;
import java.time.*;
import java.time.format.*;
import java.util.*;
import java.util.concurrent.*;
import static java.time.temporal.ChronoField.*;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;
import static java.util.stream.Collectors.toCollection;

public class FunstreamTvClient {
  private static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
      .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
      .appendLiteral('-')
      .appendValue(MONTH_OF_YEAR, 2)
      .appendLiteral('-')
      .appendValue(DAY_OF_MONTH, 2)
      .appendLiteral(' ')
      .appendValue(HOUR_OF_DAY, 2)
      .appendLiteral(':')
      .appendValue(MINUTE_OF_HOUR, 2)
      .appendLiteral(':')
      .appendValue(SECOND_OF_MINUTE, 2)
      .toFormatter();

  private final Logger logger = LoggerFactory.getLogger(FunstreamTvClient.class);

  private final Socket socket;

  private final FunstreamTvEventListener eventListener;

  private final Set<String> joinChannels = new HashSet<>();

  private final Map<String, Stream> streamMetadata = Maps.newHashMap();

  private final ExecutorService offlineExecutorService = Executors.newFixedThreadPool(3);

  public FunstreamTvClient(FunstreamTvEventListener eventListener, List<String> joinChannels) {
    this.eventListener = eventListener;
    this.joinChannels.addAll(joinChannels);

    IO.Options options = new IO.Options();
    options.reconnection = true;
    options.transports = new String[]{"websocket"};

    try {
      this.socket = IO.socket("http://funstream.tv:3811/", options);
    } catch (URISyntaxException exception) {
      //shouldn't happen
      throw new RuntimeException(exception);
    }

    socket.on(Socket.EVENT_CONNECT, args -> {
      eventListener.onConnect();
      loginAndAutoJoinRooms();
    });
    socket.on(Socket.EVENT_DISCONNECT, args -> eventListener.onDisconnect());
    socket.on("/chat/message", args ->
        Arrays.stream(args)
            .map(o -> (JSONObject) o) //since we cannot cast an array, we cast each array element
            .filter(o -> {
              try {
                logger.debug("Processing online message from channel " + o.getString("channel"));
                return streamMetadata.containsKey(o.getString("channel"));
              } catch (Exception e) {
                logger.error("Unexpected exception", e);
                return false;
              }
            })
            .forEach((o) -> {
              try {
                final Stream channel = streamMetadata.get(o.getString("channel"));
                final Message message = translateMessage(o, channel);
                eventListener.onMessage(message);
              } catch (Exception e) {
                logger.error("Unexpected exception", e);
              }
            }));
  }

  private void loginAndAutoJoinRooms() {
    try {
      //compose and emit login event
      JSONObject loginObject = new JSONObject();
      loginObject.put("token", JSONObject.NULL);
      socket.emit("/chat/login", loginObject);
      //join to auto join channels
      joinChannels.forEach(this::joinChannel);
    } catch (Exception e) {
      logger.error("Unexpected exception", e);
    }
  }

  public void connect() {
    socket.connect();
  }

  public void join(@NotNull Stream stream) {
    final String channel = "stream/" + stream.getStreamer().getId();
    this.streamMetadata.put(channel, stream);
    this.joinChannels.add(channel);
    joinChannel(channel);
  }


  private void joinChannel(final String channel) {
    try {
      JSONObject joinObject = new JSONObject();
      //if channel is main, we send empty object
      if (!"main".equals(channel)) {
        joinObject.put("channel", channel);
      }
      socket.emit("/chat/join", joinObject, (Ack) args -> {
        offlineExecutorService.submit(() -> {
          JSONObject data = (JSONObject) args[0];
          String status = data.optString("status");
          if ("ok".equals(status)) {
            JSONArray messages = data.optJSONArray("result");
            eventListener.onJoin(channel);
            logger.debug("Processing offline messages from channel " + channel + " totally " +
                messages.length());
            final Stream stream = streamMetadata.get(channel);
            if (stream != null) {
              //we have to iterate through JSONArray this way because there's no other way
              for (int i = 0; i < messages.length(); ++i) {
                eventListener.onHistoryMessage(
                    translateMessage(messages.optJSONObject(i), stream));
              }
            }
          }
        });
      });
    } catch (Exception e) {
      logger.error("Unexpected exception", e);
    }
  }

  private Message translateMessage(JSONObject object, Stream stream) {
    long id = object.optLong("id");
    String channel = object.optString("channel");
    JSONObject fromObject = object.optJSONObject("from");
    String fromName = fromObject.optString("name");
    long fromId = fromObject.optLong("id");
    User from = new User(fromName, fromId);
    JSONObject toObject = object.optJSONObject("to");
    User to = null;
    if (toObject != null) {
      String toName = toObject.optString("name");
      long toId = toObject.optLong("id");
      if (!fromName.isEmpty()) {
        to = new User(toName, toId);
      }
    }
    String text = object.optString("text");
    Instant time = LocalDateTime
        .parse(object.optString("time"), DATE_TIME_FORMATTER)
            //we have to adjust time zone because TZ information isn't provided
        .toInstant(ZoneOffset.ofHours(+2));

    final List<String> process = NLPProcessor.process(text);
    final ArrayList<String> stems = process.stream().collect(toCollection(ArrayList::new));
    return new Message(id, channel, from, to, text, stems, toStoredStream(stream), time);
  }

  public life.bigpeka.Stream toStoredStream(Stream retrievedStream) {
    return new life.bigpeka.Stream(
        retrievedStream.getId(),
        retrievedStream.getName(),
        retrievedStream.getDescription(),
        retrievedStream.getImage(),
        toStoredUser(retrievedStream.getStreamer()));
  }

  public life.bigpeka.User toStoredUser(Streamer streamer) {
    return new User(streamer.getName(), streamer.getId());
  }

  public void leave(@NotNull Stream stream) {
    final String channel = "stream/" + stream.getStreamer().getId();
    this.joinChannels.remove(channel);
    try {
      JSONObject leaveObject = new JSONObject();
      //if channel is main, we send empty object
      if (!"main".equals(channel)) {
        leaveObject.put("channel", channel);
      }
      socket.emit("/chat/leave", leaveObject, (Ack) args -> {});
    } catch (JSONException e) {
      logger.warn("unexpected json exception", e);
    }
  }
}