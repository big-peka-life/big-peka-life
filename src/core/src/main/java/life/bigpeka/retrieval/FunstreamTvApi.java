package life.bigpeka.retrieval;

import feign.*;
import feign.jackson.*;
import feign.jaxrs.JAXRSContract;
import feign.slf4j.Slf4jLogger;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("api")
public interface FunstreamTvApi {

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("content")
  public Streams getStreams(StreamsRequest data);

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("stream")
  public Stream getStream(StreamRequest data);

  public static class factories {
    public static FunstreamTvApi create() {
      return Feign.builder()
          .contract(new JAXRSContract())
          .decoder(new JacksonDecoder())
          .encoder(new JacksonEncoder())
          .logger(new Slf4jLogger())
          .logLevel(Logger.Level.BASIC)
          .target(FunstreamTvApi.class, "http://funstream.tv/");
    }
  }
}