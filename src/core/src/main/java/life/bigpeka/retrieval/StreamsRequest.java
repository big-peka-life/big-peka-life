package life.bigpeka.retrieval;

public class StreamsRequest {
  private final Category category;

  private final String content;

  private final String type;

  public StreamsRequest(String category, String content, String type) {
    this.category = new Category(category);
    this.content = content;
    this.type = type;
  }

  public Category getCategory() {
    return category;
  }

  public String getContent() {
    return content;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return "StreamsRequestEntity{" +
        "category=" + category +
        ", content='" + content + '\'' +
        ", type='" + type + '\'' +
        '}';
  }
}
