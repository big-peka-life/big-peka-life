package life.bigpeka.retrieval;

import com.google.common.collect.Sets;
import org.slf4j.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toSet;

public class FunstreamTvStreamTracker {
  private final Logger logger = LoggerFactory.getLogger(FunstreamTvStreamTracker.class);

  private final ScheduledExecutorService executor =
      Executors.newSingleThreadScheduledExecutor();

  private final Set<Stream> onlineStreams = new HashSet<>();

  private Consumer<Stream> streamOnlineCallback = s -> {};

  private Consumer<Stream> streamOfflineCallback = s -> {};

  public void start(long period, TimeUnit timeUnit) {
    executor.scheduleAtFixedRate(() -> {
      try {
        Streams response = FunstreamTvApi
            .factories
            .create()
            .getStreams(new StreamsRequest("top", "stream", "all"));
        final Set<Stream> streams = response.getStreams().stream().collect(Collectors.toSet());
        if (onlineStreams.isEmpty()) {
          onlineStreams.addAll(streams);
          invokeCallback(onlineStreams, streamOnlineCallback);
        } else {
          Set<Stream> newStreams = Sets.difference(streams, onlineStreams).immutableCopy();
          Set<Stream> wentOffline = Sets.difference(onlineStreams, streams).immutableCopy();
          onlineStreams.removeAll(wentOffline);
          onlineStreams.addAll(newStreams);
          invokeCallback(newStreams, streamOnlineCallback);
          invokeCallback(wentOffline, streamOfflineCallback);
        }
      } catch (Exception e) {
        logger.warn("unexpected exception", e);
      }
    }, 0, period, timeUnit);
  }

  public FunstreamTvStreamTracker onStreamOnline(Consumer<Stream> streamOnlineCallback) {
    this.streamOnlineCallback = streamOnlineCallback;
    return this;
  }

  public FunstreamTvStreamTracker onStreamOffline(Consumer<Stream> streamOfflineCallback) {
    this.streamOfflineCallback = streamOfflineCallback;
    return this;
  }

  private void invokeCallback(Set<Stream> streams, Consumer<Stream> callback) {
    streams.forEach(callback);
  }
}