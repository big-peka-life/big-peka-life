package life.bigpeka.retrieval;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stream {
  /**
   * {
       streamer: <obj> {...same data as /user api request...},
       name: <string> stream name,
       description: <string> stream description,
       category: <obj> {...same data as /stream/category api request, no options...},
       image: <string> url of preview image,
       thumbnail: <string> url of the player thumbnail image,
       adult: <bool> adult flag,
       rating: <int> current rating of the stream,
       players: <array|null> [
       ...,
       {
       name: <string> name of player provider, e.g. twitch,
       channel: <string> name of channel for this provider
       code: <string> html code of player
       },
       ...,
       ]
       }
   *
   */
  private final Streamer streamer;

  private final String name;

  private final int rating;

  private final String description;

  private final long id;

  private final String image;

  public Stream(
      @JsonProperty("streamer") Streamer streamer,
      @JsonProperty("name") final String name,
      @JsonProperty("rating") final int rating,
      @JsonProperty("description") final String description,
      @JsonProperty("id") final long id,
      @JsonProperty("image") final String image) {
    this.streamer = streamer;
    this.name = name;
    this.rating = rating;
    this.description = description;
    this.id = id;
    this.image = image;
  }

  public Streamer getStreamer() {
    return streamer;
  }

  public String getDescription() {
    return description;
  }

  public long getId() {
    return id;
  }

  public String getImage() {
    return image;
  }

  public String getName() {
    return name;
  }

  public int getRating() {
    return rating;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final Stream stream = (Stream) o;

    return !(streamer != null ? !streamer.equals(stream.streamer) : stream.streamer != null);
  }

  @Override
  public int hashCode() {
    // streams are qeual of streamers are equal (stream/streamerId stuff)
    return streamer != null ? streamer.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Stream{" +
        "streamer=" + streamer +
        ", name='" + name + '\'' +
        ", rating=" + rating +
        ", description='" + description + '\'' +
        ", id=" + id +
        ", image='" + image + '\'' +
        '}';
  }
}
