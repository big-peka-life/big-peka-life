package life.bigpeka.retrieval;

public class StreamRequest {
  private final long id;

  public StreamRequest(final long id) {
    this.id = id;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }
    final StreamRequest that = (StreamRequest) o;
    return id == that.id;
  }

  public long getId() {
    return id;
  }

  @Override
  public String toString() {
    return "StreamRequest{" +
        "id=" + id +
        '}';
  }
}
