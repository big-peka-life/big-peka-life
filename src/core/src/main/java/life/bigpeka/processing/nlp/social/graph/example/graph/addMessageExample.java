package life.bigpeka.processing.nlp.social.graph.example.graph;

import com.google.common.collect.Lists;
import life.bigpeka.processing.nlp.GraphProcessor;

import java.util.ArrayList;

public class addMessageExample {
    public static void main(String[] args) {
        final ArrayList<String> words = Lists.newArrayList(
                "слава", "украине", "героям", "путин", "была", "даром", "лишь");
        GraphProcessor graphProc = new GraphProcessor();
        graphProc.addressedMessageProcessing("kuzmalex", "sck_spb", words);
        System.out.print(graphProc.getGraph().toString());
    }
}
