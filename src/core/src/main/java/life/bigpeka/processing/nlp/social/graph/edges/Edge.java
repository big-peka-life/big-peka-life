package life.bigpeka.processing.nlp.social.graph.edges;

import org.jgrapht.graph.DefaultEdge;
import life.bigpeka.processing.nlp.social.graph.vertices.Vertex;

public class Edge extends DefaultEdge {
    private int messageCount = 0;

    //Возвращает силу связи, на данный момент сила связи равна количеству сообщений.
    public int getWeight() {
        return messageCount;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public void incrementMessageCount() {
        messageCount++;
    }

    @Override
    public String toString() {
        Vertex source = (Vertex) this.getSource();
        Vertex target = (Vertex) this.getTarget();
        return "Edge{source - " + source.getName() + " target - " + target.getName() + " messageCount = " + messageCount + " weight = " + getWeight() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge edge = (Edge) o;

        if (messageCount != edge.messageCount) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode()+messageCount;
    }
}
