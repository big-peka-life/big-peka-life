package life.bigpeka.processing.nlp;

import life.bigpeka.processing.nlp.social.graph.SocialGraph;
import life.bigpeka.processing.nlp.social.graph.vertices.User;

import java.util.List;

public class GraphProcessor {

    private SocialGraph graph = new SocialGraph();

    /**Обрабытывает сообщение пользователя, адресованное другому пользователю.
     *
     * @param userFromName Имя пользователя, отправившего сообщение
     * @param userToName Имя пользователя, которому адресовано сообщение
     * @param stem Лист сообщений пользователя
     */
    public void addressedMessageProcessing(String userFromName, String userToName, List<String> stem) {

        if (!graph.isVertexInGraph(userFromName, User.class)) {
            graph.addVertex(userFromName, User.class);
        }
        if (!graph.isVertexInGraph(userToName, User.class)) {
            graph.addVertex(userToName, User.class);
        }
        graph.ifNotExistAddEdge(userFromName, userToName, User.class);
        graph.incrementMessageCountInEdge(userFromName, userToName, User.class);
        graph.setWordsToUser(userFromName, stem);
    }

    /**Обрабытывает безадресное сообщение пользователя.
     *
     * @param userFromName Имя пользователя
     * @param stem Лист сообщений пользователя
     */
    public void unaddressedMessageProcessing(String userFromName, List<String> stem) {
        if (!graph.isVertexInGraph(userFromName, User.class)) {
            graph.addVertex(userFromName, User.class);
        }
        graph.setWordsToUser(userFromName, stem);
    }

    public SocialGraph getGraph(){
        return graph;
    }

}
