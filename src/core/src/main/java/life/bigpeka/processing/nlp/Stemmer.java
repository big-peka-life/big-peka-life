package life.bigpeka.processing.nlp;

import com.google.common.collect.Lists;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.russianStemmer;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

class Stemmer {
  final static ThreadLocal<SnowballStemmer> stemmer
      = ThreadLocal.withInitial(russianStemmer::new);

  public static ArrayList<String> stem(ArrayList<String> inputString) {
    final ArrayList<String> output = Lists.newArrayList();

    int character;
      for (String str : inputString){
          stemmer.get().setCurrent(str);
          stemmer.get().stem();
          output.add(stemmer.get().getCurrent());
      }

    return output;
  }
}