package life.bigpeka.processing.nlp;

import java.io.InputStream;
import java.util.*;
import java.util.regex.*;

class StopWordsCleaner {
  private static final String STOP_WORDS_RESOURCE = "/stopwords_ru.txt";

  private static ArrayList<Pattern> patterns = retrieveStopWords();

  /**
   * Removes stop words in place
   */

  public static ArrayList<String> cleanListInplace(ArrayList<String> srcWords){
      for (int i = 0; i < srcWords.size(); i++) {
          for (Pattern pattern : patterns) {
              Matcher m = pattern.matcher(srcWords.get(i));
              if (m.matches()) {
                  srcWords.remove(i);
                  i--;
                  break;
              }
          }
      }
      return srcWords;

  }

  public static ArrayList<String> clean(ArrayList<String> srcWords) {
      return cleanListInplace(srcWords);
  }

  private static ArrayList<Pattern> retrieveStopWords () {

      final InputStream resourceAsStream = StopWordsCleaner.class.getResourceAsStream(
              STOP_WORDS_RESOURCE);
      final Scanner s = new Scanner(resourceAsStream, "UTF-8");
      ArrayList<Pattern> patterns = new ArrayList<Pattern>();
      while (s.hasNext()) {
          patterns.add(Pattern.compile("((в)|(с)|(под)|(над)|(из)|(пере)|(про)|(одно)|(дву)|(само)|(не)|(ни)|(все)|(по)|" +
                                       "(до)|(изо)|(на)|(обо)|(подо)|(низ)|(еже)|(де)|(взаимо)|(внутри)|(безо)|(без))?"
                                       + s.next() +
                                       "((а)|(я)|(о)|(е)|(ь)|(и)|(ы)|(а)|(ая)|(яя)|(ое)|(ее)|(ый)" +
                                       "|(ать)|(ять)|(еть)|(уть)|(у)|(я)|(ья)|(ом)|(ит)|(ов)|(ев)|(ся)" +
                                       "|(ю)|(ем)|(ешь)|(ете)|(ет)|(ут)|(ют)|(ал)|(ял)|(ала)|(яла)|(ит)|(ом)" +
                                       "|(али)|(яли)|(ул)|(ула)|(ули)|(ек)|(ок)|(ек)|(ик)|(ник)|(чик))?" +
                                       "||([a-zа-я]?[a-zа-я]?)"));
      }
      if (patterns.size() == 0) {
          throw new RuntimeException();
      }
      return patterns;
  }
 }
