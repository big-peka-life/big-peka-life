package life.bigpeka.processing.nlp.social.graph;

import org.jgrapht.graph.DirectedMultigraph;
import life.bigpeka.processing.nlp.social.graph.edges.Edge;
import life.bigpeka.processing.nlp.social.graph.edges.EdgeFactory;
import life.bigpeka.processing.nlp.social.graph.vertices.Channel;
import life.bigpeka.processing.nlp.social.graph.vertices.User;
import life.bigpeka.processing.nlp.social.graph.vertices.Vertex;
import life.bigpeka.processing.nlp.social.graph.vertices.VertexFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
Данная реализация основана на направленном мультиграфе. Вершинами графа
являются пользователи и каналы. Ребра графа предствляют собой два типа связи:
ползователь - ползователь и ползователь - канал.
 */

public class SocialGraph {

    private DirectedMultigraph<Vertex, Edge> graph =
            new DirectedMultigraph<Vertex, Edge>(Edge.class);

    private Map<String, Vertex> users = new HashMap<String, Vertex>();
    private Map<String, Vertex> channels = new HashMap<String, Vertex>();

    /**
     * Добавляет вершину в граф.
     *
     * @param name        Имя вершины
     * @param vertexClass Класс вершины
     */
    public void addVertex(String name, Class vertexClass) {
        Vertex vertex = VertexFactory.getVertex(name, vertexClass);
        getVertices(vertexClass).put(name, vertex);
        graph.addVertex(vertex);
    }

    /**
     * Удаляет вершину из графа.
     *
     * @param name        Имя вершины
     * @param vertexClass Класс вершины
     */
    public void removeVertex(String name, Class vertexClass) {
        graph.removeVertex(getVertices(vertexClass).get(name));
        getVertices(vertexClass).remove(name);
    }

    /**
     * Создает связь в графе, если таковой еще нет.
     *
     * @param sourceName  Имя вершины, от которой направлена связь
     * @param targetName  Имя вершины, на которою направлена связь
     * @param targetClass Класс вершины, на которую направлена связь
     */
    public void ifNotExistAddEdge(String sourceName, String targetName, Class targetClass) {

        Vertex sourceVertex = users.get(sourceName);
        Vertex targetVertex = getVertices(targetClass).get(targetName);
        if (!graph.containsEdge(sourceVertex, targetVertex)) {
            Edge edge = EdgeFactory.getEdge(targetClass);
            graph.addEdge(sourceVertex, targetVertex, edge);
        }
    }

    /**
     * Возвращает коллекцию вершин определенного типа.
     *
     * @param vertexClass Класс запрашиваемых вершин
     */
    private Map<String, Vertex> getVertices(Class vertexClass) {
        if (vertexClass == Channel.class) {
            return channels;
        }
        if (vertexClass == User.class) {
            return users;
        }
        return null;
    }

    /**
     * Добавляет слова в коллекцию слов пользователя.
     *
     * @param userName Имя пользователя
     * @param words    Лист слов
     */
    public void setWordsToUser(String userName, List<String> words) {
        User user = (User) users.get(userName);
        user.setWords(words);
    }

    /**
     * Проверяет наличие вершины в графе. Принимает имя вершины и ее класс.
     *
     * @param name        Имя вершины
     * @param vertexClass Класс вершины
     * @return true если вершина уже существует, иначе false
     */
    public boolean isVertexInGraph(String name, Class vertexClass) {
        return getVertices(vertexClass).containsKey(name);
    }

    /**
     * Инкрементит счетчик сообщений в связи.
     *
     * @param sourceName  Имя вершины, от которой направлена связь
     * @param targetName  Имя вершины, на которую направлена связь
     * @param targetClass Класс вершины, на которую направлена связь
     */
    public void incrementMessageCountInEdge(String sourceName, String targetName, Class targetClass) {
        Vertex sourceVertex = users.get(sourceName);
        Vertex targetVertex = getVertices(targetClass).get(targetName);
        graph.getEdge(sourceVertex, targetVertex).incrementMessageCount();
    }

    public String toString() {
        String str = "SocailGraph{vertices: ";
        for (Vertex v : graph.vertexSet()) {
            str = str.concat(v.toString() + " ");
        }
        for (Edge e : graph.edgeSet()) {
            str = str.concat("edges: ");
            str = str.concat(e.toString() + " ");
        }
        str.concat("}");
        return str;
    }
}
