package life.bigpeka.processing.nlp.social.graph.vertices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class User extends Vertex {
    private Map<String, Integer> wordMap = new HashMap<String, Integer>();

    public User(String name) {
        super(name);
    }

    public void setWords(List<String> words) {
        for (String word : words) {
            if (wordMap.containsKey(word)) {
                int count = wordMap.get(word) + 1;
                wordMap.put(word, count);
            } else {
                wordMap.put(word, 1);
            }
        }
    }

    public Map<String, Integer> getWordMap() {
        return wordMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        if (wordMap != null ? !wordMap.equals(user.wordMap) : user.wordMap != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (wordMap != null ? wordMap.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" + name + " " + wordMap + "}";
    }
}
