package life.bigpeka;

import com.hubspot.dropwizard.guice.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.*;
import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class BPLApplication extends Application<BPLConfiguration> {

  @Override
  public void initialize(final Bootstrap<BPLConfiguration> bootstrap) {
    bootstrap.addBundle(GuiceBundle.<BPLConfiguration>newBuilder()
        .addModule(new BPLModule())
        .build());
    super.initialize(bootstrap);
    bootstrap.addBundle(new AssetsBundle("/webroot/", "/", "index.html"));
  }

  @Override
  public void run(final BPLConfiguration bplConfiguration, final Environment environment)
  throws Exception {
    environment.jersey().packages("life.bigpeka.web.services");
    environment.getApplicationContext()
        .addFilter(CrossDomainFilter.class, "/*",
            EnumSet.of(DispatcherType.REQUEST,DispatcherType.ERROR));
  }

  public static void main(String[] args)
  throws Exception {
    String[] argsForApp = {"server", "config/bpl-config-dev.yml"};
    if (args.length == 1) {
      argsForApp[1] = args[0];
    }
    new BPLApplication().run(argsForApp);
  }
}