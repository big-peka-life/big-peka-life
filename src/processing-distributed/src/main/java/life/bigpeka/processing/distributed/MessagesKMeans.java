//package life.bigpeka.processing.distributed;
//
//import life.bigpeka.Message;
//import life.bigpeka.CSVMessageSerializer;
//import org.apache.spark.api.java.*;
//import org.apache.spark.mllib.clustering.*;
//import org.apache.spark.mllib.feature.*;
//import java.util.*;
//
//public class MessagesKMeans {
//
//  private final JavaSparkContext sparkContext;
//
//  public MessagesKMeans(final JavaSparkContext sparkContext) {
//    this.sparkContext = sparkContext;
//  }
//
//  public void compute() {
//    final JavaRDD<List<String>> data = sparkContext.textFile("data")
//        .map(CSVMessageSerializer::createMessageFromCsvLine)
//        .map(optional -> {
//          if (optional.isPresent()) {
//            final Message csvMessage = optional.get();
//            return csvMessage.getStems();
//          } else {
//            return Collections.emptyList();
//          }
//        });
//
//    final HashingTF hashingTF = new HashingTF();
//    final JavaRDD<org.apache.spark.mllib.linalg.Vector> tf = hashingTF.transform(data);
//    tf.cache();
//    final IDFModel idf = new IDF(5).fit(tf);
//    final JavaRDD<org.apache.spark.mllib.linalg.Vector> tfidf = idf.transform(tf);
//
//    int clusters = 2;
//    int maxIteration = 25;
//    final KMeansModel train = KMeans$.MODULE$.train(tfidf.rdd(),
//        clusters,
//        maxIteration,
//        1,
//        KMeans$.MODULE$.K_MEANS_PARALLEL());
//    final org.apache.spark.mllib.linalg.Vector[] vectors = train.clusterCenters();
////    Arrays.stream(vectors)
////        .forEach(vector -> {
////          System.out.println(vector.numNonzeros());
////        });
//  }
//
//  public static void main(String[] args) {
//    final MessagesKMeans messagesKMeans = new MessagesKMeans(SparkUtils.createDefault());
//    messagesKMeans.compute();
//  }
//}