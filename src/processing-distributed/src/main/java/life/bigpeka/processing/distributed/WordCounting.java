//package life.bigpeka.processing.distributed;
//
//import life.bigpeka.Message;
//import life.bigpeka.CSVMessageSerializer;
//import org.apache.spark.api.java.*;
//import scala.Tuple2;
//import wordcloud.*;
//import wordcloud.bg.*;
//import wordcloud.font.*;
//import wordcloud.font.scale.LinearFontScalar;
//import wordcloud.palette.ColorPalette;
//import java.awt.*;
//import java.io.*;
//import java.util.*;
//import java.util.List;
//import static java.util.stream.Collectors.toCollection;
//
//public class WordCounting {
//
//  private final JavaSparkContext sparkContext;
//
//  public WordCounting(final JavaSparkContext sparkContext) {
//    this.sparkContext = sparkContext;
//  }
//
//  public List<Tuple2<String, Integer>> performCounting(final String path) {
//    final JavaPairRDD<String, Integer> stemsToOnes = sparkContext.textFile(path)
//        .map(CSVMessageSerializer::createMessageFromCsvLine)
//        .flatMapToPair(optional -> {
//          if (optional.isPresent()) {
//            final Message message = optional.get();
//            final List<String> stems = message.getStems();
//            return stems.stream()
//                .map(s -> new Tuple2<>(s, 1))
//                .collect(toCollection(ArrayList::new));
//          } else {
//            return Collections.emptyList();
//          }
//        });
//    return stemsToOnes.aggregateByKey(0,
//        (v1, v2) -> v1 + v2,
//        (v11, v21) -> v11 + v21)
//        .collect();
//  }
//
//  public static void main(String[] args)
//  throws IOException {
//    final JavaSparkContext sparkContext = SparkUtils.createDefault();
//    final List<Tuple2<String, Integer>> res =
//        new WordCounting(sparkContext).performCounting("messages.csv");
//    sparkContext.stop();
//
//    final ArrayList<WordFrequency> wordFrequencies = res.stream()
//        .sorted((a, b) -> b._2() - a._2())
//        .limit(500)
//        .map((e) -> new WordFrequency(e._1(), e._2()))
//        .collect(toCollection(ArrayList::new));
//
//        drawRectangularCloud(wordFrequencies);
////    drawPekaCloud(wordFrequencies);
//  }
//
//  private static void drawRectangularCloud(final ArrayList<WordFrequency> wordFrequencies) {
//    final WordCloud wordCloud = new WordCloud(1200, 400, CollisionMode.RECTANGLE);
//    wordCloud.setPadding(0);
//    wordCloud.setBackground(new RectangleBackground(1200, 400));
//    wordCloud.setCloudFont(new CloudFont("Arial", FontWeight.PLAIN));
//    wordCloud.setFontScalar(new LinearFontScalar(30, 148));
//    wordCloud.build(wordFrequencies);
//    wordCloud.writeToFile("cloud.png");
//  }
//
//  private static void drawPekaCloud(final ArrayList<WordFrequency> wordFrequencies)
//  throws IOException {
//    final WordCloud wordCloud = new WordCloud(1979, 1979, CollisionMode.PIXEL_PERFECT);
//    wordCloud.setPadding(2);
//    final FileInputStream fileInputStream = new FileInputStream(new File("large-peka.png"));
//    wordCloud.setCloudFont(new CloudFont("Arial", FontWeight.PLAIN));
//    wordCloud.setBackground(new PixelBoundryBackground(fileInputStream));
//    wordCloud.setFontScalar(new LinearFontScalar(40, 300));
//    wordCloud.setColorPalette(new ColorPalette(
//        new Color(0x4055F1),
//        new Color(0x408DF1),
//        new Color(0x40AAF1),
//        new Color(0x40C5F1),
//        new Color(0x40D3F1),
//        new Color(0xFFFFFF)));
//    wordCloud.build(wordFrequencies);
//    wordCloud.writeToFile("peka-cloud.png");
//  }
//}