//package life.bigpeka.processing.distributed;
//
//import life.bigpeka.SampleMessages;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.junit.Test;
//import scala.Tuple2;
//import java.util.*;
//import static java.util.stream.Collectors.toCollection;
//import static org.assertj.core.api.Assertions.assertThat;
//
//public class WordCountingTest {
//
//  @Test
//  public void shouldPerformCountingForSampleMessages(){
//    final JavaSparkContext aDefault = SparkUtils.createDefault();
//    final WordCounting wordCounting = new WordCounting(aDefault);
//
//    final List<Tuple2<String, Integer>> results = wordCounting
//        .performCounting(SampleMessages.getPathAsString());
//
//    assertThat(results).isNotNull();
//    final ArrayList<Tuple2<String, Integer>> collect = results.stream()
//        .filter((e) -> "peka".equals(e._1()))
//        .collect(toCollection(ArrayList::new));
//    assertThat(collect.size()).isEqualTo(1);
//    assertThat(collect.get(0)._2()).isEqualTo(4);
//  }
//}