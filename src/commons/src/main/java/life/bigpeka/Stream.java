package life.bigpeka;

import org.mongodb.morphia.annotations.*;

@Entity("streams")
public class Stream {
  @Id
  private long id;

  private String name;

  private String description;

  private String image;

  @Reference
  private User streamer;

  public Stream(final long id, final String name, final String description, final String image,
      final User streamer) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.image = image;
    this.streamer = streamer;
  }

  public Stream() {

  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getImage() {
    return image;
  }

  public User getStreamer() {
    return streamer;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final Stream stream = (Stream) o;

    if (id != stream.id) { return false; }
    if (name != null ? !name.equals(stream.name) : stream.name != null) { return false; }
    if (description != null ? !description.equals(stream.description) :
        stream.description != null) {
      return false;
    }
    if (image != null ? !image.equals(stream.image) : stream.image != null) { return false; }
    return !(streamer != null ? !streamer.equals(stream.streamer) : stream.streamer != null);
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (image != null ? image.hashCode() : 0);
    result = 31 * result + (streamer != null ? streamer.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Stream{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", image='" + image + '\'' +
        ", streamer=" + streamer +
        '}';
  }
}
