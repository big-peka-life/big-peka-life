package life.bigpeka;

import com.google.common.base.*;
import com.google.common.collect.Lists;
import com.mongodb.*;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;
import java.io.Serializable;
import java.time.*;
import java.util.*;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.*;

@Entity("messages")
public class Message implements Serializable {
  @Id
  private long id;

  private String channel;

  @Reference
  private User from;

  @Reference
  private User to;

  @Reference
  private Stream stream;

  private String text;

  private List<String> stems;

  //@todo что сделать, чтобы не ругалось No usable constructor for java.time.Instant при вычетке?
  @Transient
  private Instant time;

  //@todo Сделать без String-овых констант
  @PostLoad
  public void postLoad(DBObject dbObj) {
    BasicDBObject timeDBObject = (BasicDBObject)dbObj.get("time");
    time = Instant.ofEpochSecond(timeDBObject.getLong("seconds"), timeDBObject.getLong("nanos"));
  }

  public Message(final long id, final String channel, final User from,
      final User to, final String text, final List<String> stems,
      final Stream stream, final Instant time) {
    this.id = id;
    this.channel = channel;
    this.from = from;
    this.to = to;
    this.text = text;
    this.stems = stems;
    this.time = time;
    this.stream = stream;
  }

  private Message() {

  }

  public long getId() {
    return id;
  }

  public String getChannel() {
    return channel;
  }

  public User getFrom() {
    return from;
  }

  public User getTo() {
    return to;
  }

  public String getText() {
    return text;
  }

  public Instant getTime() {
    return time;
  }

  public List<String> getStems() {
    return stems;
  }

  public Stream getStream() {
    return stream;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final Message message = (Message) o;

    if (id != message.id) { return false; }
    if (channel != null ? !channel.equals(message.channel) : message.channel != null) {
      return false;
    }
    if (from != null ? !from.equals(message.from) : message.from != null) { return false; }
    if (to != null ? !to.equals(message.to) : message.to != null) { return false; }
    if (text != null ? !text.equals(message.text) : message.text != null) { return false; }
    if (stems != null ? !stems.equals(message.stems) : message.stems != null) { return false; }
    return !(time != null ? !time.equals(message.time) : message.time != null);
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (channel != null ? channel.hashCode() : 0);
    result = 31 * result + (from != null ? from.hashCode() : 0);
    result = 31 * result + (to != null ? to.hashCode() : 0);
    result = 31 * result + (text != null ? text.hashCode() : 0);
    result = 31 * result + (stems != null ? stems.hashCode() : 0);
    result = 31 * result + (time != null ? time.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Message{" +
        "id=" + id +
        ", channel='" + channel + '\'' +
        ", from=" + from +
        ", to=" + to +
        ", text='" + text + '\'' +
        ", stems=" + stems +
        ", time=" + time +
        '}';
  }
}
