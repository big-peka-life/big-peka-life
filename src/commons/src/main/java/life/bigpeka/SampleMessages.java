package life.bigpeka;

import com.google.common.io.Resources;
import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.List;

/**
 * Messages file retrieved from resources for testing
 */
public class SampleMessages {
  private SampleMessages() {

  }

  public static File getAsFile() {
    try {
      return new File(Resources.getResource("messages.csv").toURI());
    } catch (URISyntaxException e) {
      throw new RuntimeException();
    }
  }

  public static String getPathAsString() {
    return getAsFile().getAbsolutePath();
  }
}