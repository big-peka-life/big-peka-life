package life.bigpeka;

import org.mongodb.morphia.annotations.*;
import java.util.Objects;

@Entity("users")
public class User {
  @Id
  private long id;

  @Indexed()
  private String name;

  public User(final String name, final long id) {
    this.name = name;
    this.id = id;
  }

  private User() {

  }

  public String getName() {
    return name;
  }

  public long getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, id);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }
    final User that = (User) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(name, that.name);
  }

  @Override
  public String toString() {
    return "FunstreamTvUser{" +
        "name='" + name + '\'' +
        ", id=" + id +
        '}';
  }
}